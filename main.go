package main

import (
	"github.com/hajimehoshi/ebiten"
	"image/color"
	"strings"
)

type Grid struct {
	rows int
	columns int
	cells []Cell
}

func PrepareGrid(rows int, columns int) []Cell {
	grid := make([]Cell, rows*columns)
	for i := 0; i < rows*columns; i++ {
		c := new(Cell)
		currentRow := i / columns
		currentColumn := i % rows
		c.Initialize(currentRow, currentColumn)
		grid[i] = *c
	}

	return grid
}

func (g *Grid) Initialize(rows int, columns int) {
	g.rows = rows
	g.columns = columns

	g.cells = PrepareGrid(rows, columns)
}

type Cell struct {
	row int
	column int
	north *Cell
	east *Cell
	south *Cell
	west *Cell
	links map[string]bool
}

func (cell *Cell) Initialize(row int, column int) {
	cell.row = row
	cell.column = column
}

func (cell *Cell) Link(cellToAdd *Cell) {
	cellName := GetCellName(cellToAdd)
	cell.links[cellName] = true
}

func (cell *Cell) Unlink(cellToRemove *Cell) {
	cellName := GetCellName(cellToRemove)
	delete(cell.links, cellName)
}

func (cell *Cell) Links() []string {
	keys := make([]string, 0, len(cell.links))
	for k := range cell.links {
		keys = append(keys, k)
	}
	return keys
}

func (cell *Cell) Linked(linkedCell *Cell) bool {
	_, err := cell.links[GetCellName(linkedCell)]
	if err {
		return false
	}
	return true
}

func (cell *Cell) Neighbors() []Cell {
	neighborCells := make([]Cell, 0)
	if cell.north != nil {
		neighborCells = append(neighborCells, *cell.north)
	}
	if cell.south != nil {
		neighborCells = append(neighborCells, *cell.south)
	}
	if cell.east != nil {
		neighborCells = append(neighborCells, *cell.east)
	}
	if cell.west != nil {
		neighborCells = append(neighborCells, *cell.west)
	}

	return neighborCells
}

func GetCellName(cell *Cell) string {
	return strings.Join([]string{string(cell.row), string(cell.column)}, "")
}

type Game struct {}

func (g *Game) Update(screen *ebiten.Image) error {
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	screen.Fill(color.White)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return 640, 480
}

func main() {
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Hello, World!")
	game := &Game{}
	g := new(Grid)
	g.Initialize(10, 10)
	if err := ebiten.RunGame(game); err != nil {
		panic(err)
	}
}
